CREATE DATABASE  IF NOT EXISTS `realmadrid` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `realmadrid`;
-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: localhost    Database: realmadrid
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `jugadores`
--

DROP TABLE IF EXISTS `jugadores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jugadores` (
  `idJugador` int NOT NULL,
  `nombre` varchar(70) NOT NULL,
  `edad` int NOT NULL,
  `posicion` varchar(35) NOT NULL,
  `goles` int NOT NULL,
  `salarioSemanal` double NOT NULL,
  `lesionado` tinyint(1) NOT NULL,
  `dorsal` int NOT NULL,
  PRIMARY KEY (`idJugador`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jugadores`
--

LOCK TABLES `jugadores` WRITE;
/*!40000 ALTER TABLE `jugadores` DISABLE KEYS */;
INSERT INTO `jugadores` VALUES (1,'Thibaut Courtois',28,'Portero',0,10,0,1),(2,'Andey Lunin',21,'Portero',0,1.5,0,13),(3,'Daniel Carvajal',29,'Defensa',1,7,1,2),(4,'Eder Militao',26,'Defensa',0,6,0,3),(5,'Sergio Ramos',33,'Defensa',4,15,0,4),(6,'Rapahel Varane',26,'Defensa',1,8,0,5),(7,'Nacho',31,'Defensa',0,5,1,6),(8,'Marcelo',32,'Defensa',0,11,1,12),(9,'Ferland Mendy',23,'Defensa',1,4,0,23),(10,'Alvaro Odriozila',24,'Defensa',0,2,1,19),(11,'Toni Kroos',28,'Mediocentro',2,13,0,8),(12,'Luka Modrid',33,'Mediocentro',1,14,0,10),(13,'Casemiro',28,'Mediocentro',1,9,0,14),(14,'Fede Valverde',22,'Mediocentro',3,4.5,0,15),(15,'Martin Odegaard',20,'Mediocentro',1,3,0,21),(16,'Isco',28,'Mediocentro',0,9.5,0,22),(17,'Lucas Vazquez',31,'Mediocentro',0,7.6,0,17),(18,'Eden Hazard',27,'Delantero',6,16,0,7),(19,'Marco Asensio',24,'Delantero',1,8.7,0,11),(20,'Karim Benzema',29,'Delantero',4,14.5,0,9),(21,'Luca Jovic',23,'Delantero',1,7.8,0,18),(22,'Vinicius',20,'Delantero',2,6.8,0,20),(23,'Mariano',25,'Delantero',0,7.8,0,24),(24,'Rodrigo',19,'Delantero',0,4.3,1,25);
/*!40000 ALTER TABLE `jugadores` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-11-06 13:16:07
