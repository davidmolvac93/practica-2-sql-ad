/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author david
 */
public class Conexion {
    public static final String propertiesFile = "C:\\Users\\david\\Desktop\\AD-BD\\sqlpropiedades.xml";

    public String dbms;
    public String dbName;
    public String userName;
    public String password;
    public String urlString;

    public String driver;
    public String serverName;
    public int portNumber;

    public Conexion() throws IOException {

        setProperties();
    }


    private void setProperties() throws IOException {

        
        //prop.loadFromXML(Files.newInputStream(Paths.get(fileName)));

        this.dbms = "mysql";
        this.driver = "com.mysql.jdbc.Driver;";
        this.dbName = "realmadrid";
        this.userName = "root";
        this.password = "system";
        this.urlString = "jdbc:mysql://localhost:3306/realmadrid?characterEncoding=utf8";
        this.serverName = "localhost";
        this.portNumber = 3306;

    }

    public Connection getConexion() throws SQLException, IOException {

        Connection con = DriverManager.getConnection(this.urlString, this.userName, this.password);

        System.out.println("Conectado a la BBDD");
        return con;
    }

    public void closeConnection(Connection connArg) {
       
        System.out.println("Vamos a cerrar el programa, esto se acaba");
        try {
            if (connArg != null) {
                connArg.close();
                connArg = null;
                System.out.println("Cerrado");
            }
        } catch (SQLException sqle) {
            System.err.println(sqle);
        }
}
}