/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import modelo.Conexion;

/**
 *
 * @author david
 */
public class NewMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String url = "jdbc:mysql://localhost:3306/realmadrid?characterEncoding=utf8";

            Conexion conexion = new Conexion();
            Connection con = conexion.getConexion();
            
            Statement st = con.createStatement();

            String query = "select * from jugadores";
            ResultSet rs = st.executeQuery(query);

            /*
            while (rs.next()) {
                System.out.println(rs.getString(1));
            }
            conexion.closeConnection(con);
            */
            while(rs.next()){
                System.out.println("id:" +  rs.getString(1) + ". Nombre: " + rs.getString(2) + ". Edad: " + rs.getString(3) + ". Posicion: " + rs.getString(4) + ". Goles: " + rs.getString(5) + ". Salario semanal: " + rs.getString(6) + ". Lesionado: " + rs.getString(7) + ". Dorsal: " + rs.getString(8));
            }
            conexion.closeConnection(con);
             

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
}
